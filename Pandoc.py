import sublime
import sublime_plugin

import os
import subprocess


class PandocCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.settings = sublime.load_settings('Pandoc.sublime-settings')
        convert_targets = self.settings.get('convert_targets')
        self.available_targets = [target for target in convert_targets.keys() if self.view.syntax().scope in convert_targets[target]['input_scope']] 
        self.view.window().show_quick_panel(self.available_targets, self.execute_pandoc_command)

    def execute_pandoc_command(self, selected_convert_target):
        if selected_convert_target == -1:
            return

        pandoc_binary = self.settings.get('pandoc_binary')
        if pandoc_binary is None:
            sublime.error_message('Pandoc binary not set. Configure in settings for the Pandoc plugin.')
            print('pandoc_binary not set')
            return

        target_options = self.settings.get('convert_targets')[self.available_targets[selected_convert_target]]

        cmd = [pandoc_binary]
        cmd.append('-f')
        cmd.append(target_options['input_scope'][self.view.syntax().scope])
        cmd.extend(target_options['arguments'])

        region = sublime.Region(0, self.view.size())
        contents = self.view.substr(region)

        buffer_file_name = self.view.file_name()
        if buffer_file_name:
            working_dir = os.path.dirname(buffer_file_name)
            file_name = os.path.splitext(buffer_file_name)[0]
        else:
            sublime.error_message('No file name available, save the buffer before running Pandoc.')
            print('No file name available, save the buffer before running Pandoc.')
            return

        cmd.append('-o')
        cmd.append(file_name + '.' + target_options['output_extension'])

        print(cmd)

        process = subprocess.Popen(
            cmd, 
            shell=False, cwd=working_dir,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE, 
            stderr=subprocess.PIPE)

        stdout, stderr = process.communicate(contents.encode('utf-8'))

        if stderr:
            stderr_content = stderr.decode('utf-8').strip()

            if stderr_content.startswith('[WARNING]'):
                sublime.message_dialog('Running the command:\n\n' + 
                    ' '.join(cmd) + 
                    '\n\nGave the following warning:\n' +
                    stderr_content)
            else:
                sublime.error_message('\n'.join([
                    'running ' + ' '.join(cmd),
                    stderr_content]))

            return